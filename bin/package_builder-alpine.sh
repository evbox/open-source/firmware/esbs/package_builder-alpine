#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_KEY_NAME='esbs_package_builder.rsa'

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo 'Alpine Linux package creator helper script.'
	echo '    -a  Supply an alternate path to APKBUILD.in'
	echo '    -h  Print usage'
	echo '    -k  Key pair (<filename> and <filename>.pub) or (concatenated priv and pub) key PEM format [SSL_KEY]'
	echo "    -n  Name of the key pair to use (default: ${DEF_KEY_NAME} and ${DEF_KEY_NAME}.pub) [SSL_KEY_NAME]"
	echo '    -p  Increase pkgrel with the value of this parameter (default: 0)'
	echo '    -r  Supply a comma separated list of repositories to use (prefix with comma to append existing) [REPOSITORIES]'
	echo
	echo 'All options can also be passed in environment variables (listed between [brackets]).'
}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

cleanup()
{
	if [ -n "${remind_key_cleanup:-}" ]; then
		echo 'Generated or installed keys, remember to clean or store them:'
		echo "${remind_key_cleanup}" | tr -s ' ' '\n'
		echo '-----------------------------------------------------------------'
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	src_file="$(readlink -f "${0}")"
	src_dir="${src_file%%"${src_file##*'/'}"}"

	if [ -n "${REPOSITORIES:-}" ]; then
		if [ "${REPOSITORIES#,}" = "${REPOSITORIES}" ]; then
			echo '# List of repositories manually given at build time' > '/etc/apk/repositories'
		fi
		echo "${REPOSITORIES:-}" | tr -s ' ,\f\n\t\v' '\n' >> '/etc/apk/repositories'
	fi

	if git rev-parse --show-cdup 2> '/dev/null'; then
		_untracked_files="$(git ls-files --exclude-standard --others)"
		_modified_files="$(git diff-index --name-only HEAD --)"
		if [ -n "${_modified_files:-}" ] || \
		   [ -n "${_untracked_files:-}" ]; then
			dirty='yes'
			e_warn 'The current repository is dirty!'
			${_untracked_files:+echo '** Untracked files **'}
			${_untracked_files:+echo "${_untracked_files:-}"}
			${_untracked_files:+echo '---------------------'}
			${_modified_files:+echo '** Modified files **'}
			${_modified_files:+echo "${_modified_files:-}"}
			${_modified_files:+echo '===================='}
			{
				git status
				git diff
			} > 'dirty_git_report.log'
			if [ -n "${DEBUG:-}" ]; then
				cat 'dirty_git_report.log'
			fi
		fi

		_git_last_tag="$(git describe --abbrev=0 2> '/dev/null' || true)"
		_pkgver="${_git_last_tag:-v0.0.1}"

		if [ "${_pkgver##*'-rc'}" != "${_pkgver}" ]; then
			_pkgrc="${_pkgver##*'-rc'}"
		fi

		_pkgver="${_pkgver%%'-rc'*}${dirty:+d}"
		_pkgver="${_pkgver##*'/'}${_pkgrc:+_rc${_pkgrc}}"
		_pkgver="${_pkgver#'v'}"

		_git_commit_count="$(git rev-list "${_git_last_tag}..HEAD" --count 2> '/dev/null' || true)"
		if [ "${_git_commit_count:-0}" -gt 0 ]; then
			_pkgver="${_pkgver}_p$(printf '%.5d' "${_git_commit_count}")"
		fi
	fi

	if [ -f "${apkbuild_path}" ]; then
		_cur_pkgrel="$(sed -n 's|^pkgrel=\([[:digit:]]\+\)$|\1|p' "${apkbuild_path}")"
		sed \
		    -e "s|@PKGVER@|${_pkgver:-0.0.0}|g" \
		    -e "s|^pkgrel.*|pkgrel=$((_cur_pkgrel + pkgrel))|g" \
		    "${apkbuild_path}" > 'APKBUILD'
	fi

	if [ ! -f 'APKBUILD' ]; then
		e_err 'No "APKBUILD" file found.'
		exit 1
	fi

	if [ -n "${ssl_key}" ]; then
		if [ ! -f "${ssl_key}" ]; then
			_ssl_key="$(mktemp -p "${TMPDIR:-/tmp}" 'ssl_key-XXXXXXXX')"
			touch "${_ssl_key}.pub"
			remind_key_cleanup="${remind_key_cleanup:-} '${_ssl_key}' '${_ssl_key}.pub'"

			printf '%s' "${ssl_key%%-----END PRIVATE KEY-----*}-----END PRIVATE KEY-----" | fold > "${_ssl_key}"
			printf '%s' "${ssl_key#*-----END PRIVATE KEY-----}" | sed '/./,$!d' | fold > "${_ssl_key}.pub"

			ssl_key="${_ssl_key}"
		fi
	else
		if [ -f "${ssl_key_name}" ]; then
			ssl_key="${ssl_key_name}"
		elif [ -f "${src_dir}/${ssl_key_name}" ]; then
			ssl_key="${src_dir}/${ssl_key_name}"
		elif [ -f "${HOME}/.abuild/abuild.conf" ]; then
			# shellcheck source=/dev/null
			. "${HOME}/.abuild/abuild.conf"
			_append_key='false'
		elif [ -f '/etc/abuild.conf' ]; then
			# shellcheck source=/dev/null
			. '/etc/abuild.conf'
		fi
		if [ ! -f "${ssl_key:-}" ] && \
		   [ -f "${PACKAGER_PRIVKEY:-}" ]; then
			ssl_key="${PACKAGER_PRIVKEY}"
		fi
	fi

	if ! openssl pkey \
		-check \
		-in "${ssl_key:-}" \
		-noout; then
		e_warn "No valid key '${ssl_key:-}', generating new key pair ..."
		e_warn 'Especially in docker containers, ensure these are not lost!'
		abuild-keygen -a -n -q
		_append_key='false'
		# shellcheck source=/dev/null
		. "${HOME}/.abuild/abuild.conf"
		ssl_key="${PACKAGER_PRIVKEY}"
		remind_key_cleanup="${remind_key_cleanup:-} '${ssl_key}' '${ssl_key}.pub'"
	fi

	if [ ! -f "${ssl_key:-}.pub" ]; then
		e_err "Key pair '${ssl_key:-}(.pub)' invalid, proper keys required to continue"
		exit 1
	fi

	if [ "${_append_key:-}" != 'false' ]; then
		if [ -f "${HOME}/.abuild/abuild.conf" ]; then
			sed -i -e 's/^PACKAGER_PRIVKEY=/\#&/' "${HOME}/.abuild/abuild.conf"
		fi
		mkdir -p "${HOME}/.abuild"
		echo "PACKAGER_PRIVKEY=\"${ssl_key}\"" >> "${HOME}/.abuild/abuild.conf"
	fi

	install -D -m 644 -t "/etc/apk/keys/" "${ssl_key}.pub"
	remind_key_cleanup="${remind_key_cleanup:-} '/etc/apk/keys/${ssl_key##*/}.pub'"
}

build_package()
{
	abuild -F checksum
	apk update
	nice -n 19 abuild -F -r
}

verify_package()
{
	echo 'Verifying generated packages'
	find "${HOME}/packages/" -type f -iname '*.apk' -exec apk verify '{}' \;
}

main()
{
	while getopts ':a:hk:n:p:r:' _options; do
		case "${_options}" in
		'a')
			apkbuild_path="${OPTARG}"
			;;
		'h')
			usage
			exit 0
			;;
		'k')
			ssl_key="${OPTARG}"
			;;
		'n')
			ssl_key_name="${OPTARG}"
			;;
		'p')
			pkgrel="${OPTARG}"
			;;
		'r')
			REPOSITORIES="${OPTARG}"
			;;
		':')
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		*)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	pkgrel="${PKGREL:-${pkgrel:-0}}"
	ssl_key="${ssl_key:-${SSL_KEY:-}}"
	ssl_key_name="${ssl_key_name:-${SSL_KEY_NAME:-${DEF_KEY_NAME}}}"
	apkbuild_path="${apkbuild_path:-APKBUILD.in}"

	init
	build_package
	verify_package

	if [ -d 'output/' ]; then
		rm -f -r 'output/'
	fi
	cp -a "${HOME}/packages/" 'output/'

	cleanup
}

main "${@}"

if [ -n "${dirty:-}" ]; then
	e_err 'The repository was dirty during the build.'
	exit 1
fi

exit 0
