# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


**NOTE:** DO NOT EDIT! This changelog is automatically generated. See the README.md file for more information.

## [v1.5.2] - 2023-04-12
### Fixes
  - CI: Switch to mass-linters template
  - CI: Globally disable SC2317
  - CI: Add shellcheck config
  - shellcheck: Fix pedantic shellcheck warnings
  - sh: Modernize printing
  - docker: Use full docker commands
  - shellcheck: Avoid using incorrect quotes

## [v1.5.0] - 2021-01-30
### Added
- Bump pkgrel field

## [v1.4.0] - 2020-10-25
### Added
- Allow selecting alpine version
- Inform user of repository dirtyness

## [v1.3.0] - 2020-01-14
### Fixes
- Dirty arguments

### Changed
- build multi target images

## [v1.2.0] - 2019-12-09
### Added
- Allow external keys
### Fixed
- Do not use double equals
### Changed
- Leading zero's on package version

## [v1.1.0] - 2019-11-19
### Added
- Add environment variable to override default repository list

### Changed
- Minor generic cleanups

## [v1.0.0] - 2019-09-10
### Added
- Add support to make it easier to build AlpineLinux packages
