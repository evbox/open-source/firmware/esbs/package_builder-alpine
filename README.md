# Alpine package builder
Rather then having to copy each file onto the target manually, instead it is
also possible to use an package manager. This repository contains a few scripts
to generate a docker container to build Alpine packages (apk).

This repository generates a container to be used from a CI system.

Note, that the purpose of this repository is NOT to create upstream Alpine
packages nor to compile packages using the Alpine packaging build system.

## Using this repository
This repository is not intended to be used directly. Instead, the docker image
create from this registry should be used instead. Alternatively this repository
could be forked, and used to push images to a different registry, or to build
local docker images.

## Git hooks
To ensure consistency, some git hooks are available in this repository. It is
strongly recommended to enable these.
```console
git config --local core.hooksPath .githooks
```

## Scripts
The repository contains two scripts,  to create an alpine package and validate
the build environment. While not recommended, if a native build is preferred,
the 'buildenv_check.sh' scrip can be used to validate if a package can be built.

Otherwise, the main script, `package_builder-alpine.sh` should be run and used
in the generated docker container.

### Prerequisites
This repository only requires docker and a few standard shell tools. The build
script will run a few self-tests before attempting to build anything.

### Usage
To create a local docker container for use, calling the `docker_run.sh` script
is enough. Tagging and pushing to a registry manually is not done by the script
as this is normally handled by the CI. For now the CI will only push docker
images to the registry if the branch is prefixed with the **release/** tag.

It is recommended to create a symlink in, for example `~/bin` pointing towards
`docker_run.sh`. Using `package_builder-alpine.sh` is a good idea to name
the symlink.
```console
ln -s <path_to>/docker_run.sh ~/bin/package_builder-alpine.sh
```

### Options
The `docker_run.sh` script accepts to following arguments:

* ALPINE_VERSION Select an alpine version to use, default is *latest*
* TARGET_ARCH Target architecture to use, default is *library* (native)

## Docker image
The created image can be used to use the Alpine build tools. So for example to
call *newapkbuild* only prefixing it with
`docker run --rm -it <container_name> newapkbuild` is needed. Note however that
as is normal with docker, files inside the container remain inside. This can be
solved by using bind-mounts or using docker to copy the files in/out. See the
docker manual for more information.

# Key management
The `packager_builder-alpine.sh` script will try to use the keypair found in
*${HOME}/.abuild* by copying the public key into */etc/apk/keys*. If this is not
intended or desired, ensure that keys are properly installed and this step will
be skipped.
