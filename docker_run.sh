#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_KEY_NAME='esbs_package_builder.rsa'
WORKDIR="${WORKDIR:-/workdir}"
REQUIRED_COMMANDS='
	[
	basename
	command
	docker
	echo
	eval
	exit
	getopts
	hostname
	id
	printf
	pwd
	readlink
	test
'

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

cleanup()
{
	if [ -d 'output/' ]; then
		run_script chown "$(id -u || true):$(id -g || true)" -R 'output/'
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	src_file="$(readlink -f "${0}")"
	src_dir="${src_file%%"${src_file##*'/'}"}"

	if ! docker image pull "${CI_REGISTRY_IMAGE:=unknown}" 2> '/dev/null'; then
		e_warn "Unable to pull docker image '${CI_REGISTRY_IMAGE}', building locally instead."
		# shellcheck disable=SC2021  # Busybox tr is non-posix without classes
		CI_REGISTRY_IMAGE="$(basename "${src_dir}" | tr '[A-Z]' '[a-z]'):latest"
		if ! docker image build \
		            --build-arg "ALPINE_VERSION=${ALPINE_VERSION:-latest}" \
		            --build-arg "TARGET_ARCH=${TARGET_ARCH:-library}" \
		            --pull \
		            --tag "${CI_REGISTRY_IMAGE}" "${src_dir}"; then
			e_warn 'Failed to build local image, attempting existing image.'
		fi
	fi

	if ! docker image inspect "${CI_REGISTRY_IMAGE}" 1> '/dev/null'; then
		e_err "Image '${CI_REGISTRY_IMAGE}' not found, cannot continue."
		exit 1
	fi

	opt_docker_args="${OPT_DOCKER_ARGS:-}"
	opt_docker_args="--volume '${PACKAGER_PATH:-$(pwd)}:${WORKDIR}' ${opt_docker_args}"

	if [ -z "${ssl_key}" ]; then
		if [ -f "${ssl_key_name}" ]; then
			ssl_key="${ssl_key_name}"
		elif [ -f "${src_dir}/${ssl_key_name}" ]; then
			ssl_key="${src_dir}/${ssl_key_name}"
		elif [ -f "${HOME}/.abuild/abuild.conf" ]; then
			# shellcheck source=/dev/null
			. "${HOME}/.abuild/abuild.conf"
		elif [ -f '/etc/abuild.conf' ]; then
			# shellcheck source=/dev/null
			. '/etc/abuild.conf'
		fi
		if [ ! -f "${ssl_key:-}" ] && [ -f "${PACKAGER_PRIVKEY:-}" ]; then
			ssl_key="${PACKAGER_PRIVKEY}"
		fi
	fi

	if [ -n "${ssl_key:-}" ]; then
		if [ -L "${ssl_key:-}" ]; then
			e_err 'Symlinks can only be used via the environment variable'
			e_err 'when being run in using this docker helper script.'
			exit 1
		fi

		if [ -f "${ssl_key}" ]; then
			ssl_key="$(readlink -f "${ssl_key}")"
			_key_location="$(dirname "${ssl_key}")"
			opt_docker_args="--volume '${_key_location}:${_key_location}' ${opt_docker_args}"
		fi
		opt_docker_args="--env 'SSL_KEY=${ssl_key}' ${opt_docker_args}"
	fi

	if [ -n "${ssl_key_name:-}" ]; then
		opt_docker_args="--env 'SSL_KEY_NAME=${ssl_key_name}' ${opt_docker_args}"
	fi

	for _repository in $(echo "${REPOSITORIES:-}" | tr -s ' ,\f\n\t\v' ' '); do
		if [ -z "${_repository}" ]; then
			continue
		fi

		if [ "${_repository%'~'*}" != "${_repository}" ]; then
			eval _repository="${_repository}"
		fi

		if [ -d "${_repository}" ]; then
			_repository="$(readlink -f "${_repository}")"
			opt_docker_args="--volume '${_repository}:${_repository}' ${opt_docker_args}"
		fi

		if [ -z "${_repositories:-}" ]; then
			_repositories="${_repository}"
		else
			_repositories="${_repositories},${_repository}"
		fi
	done
	repositories="${_repositories:-}"
}

run_script()
{
	eval docker container run \
	            --hostname "$(hostname || true)" \
	            --interactive \
	            --rm \
	            --tty \
	            --workdir "${WORKDIR}" \
	            "${opt_docker_args:-}" \
	            "${CI_REGISTRY_IMAGE}" \
	            "${*}"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err 'Self-test failed, missing dependencies.'
		echo '======================================='
		echo 'Passed tests:'
		# As the results contain \n, we expect these to be interpreted.
		# shellcheck disable=SC2059
		printf "${_test_result_pass:-none\n}"
		echo '---------------------------------------'
		echo "Failed tests:"
		# shellcheck disable=SC2059
		printf "${_test_result_fail:-none\n}"
		echo '======================================='
		exit 1
	fi
}

main()
{
	# Shadow options so that they can be forwarded after processing due to files
	# needing to be mounted when forwarding them into the container.
	# This will fail with symlinks as we resolve them here to forward them
	# implies there is no help for the -i parameter.
	while getopts ':k:n:' _options; do
		case "${_options}" in
		'k')
			ssl_key="${OPTARG}"
			;;
		'n')
			ssl_key_name="${OPTARG}"
			;;
		*)
			;;
		esac
	done

	ssl_key="${ssl_key:-${SSL_KEY:-}}"
	ssl_key_name="${ssl_key_name:-${SSL_KEY_NAME:-${DEF_KEY_NAME}}}"

	check_requirements
	init
	run_script "${@}" "${repositories:+-r ${append_repositories:+,}${repositories}}"
	cleanup
}

main "${@}"

exit 0
